
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'hal', component: () => import('pages/Halaman1.vue') },
      { path: 'hal2', component: () => import('pages/Halaman2.vue') },
      { path: 'hal3', component: () => import('pages/Halaman3.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
